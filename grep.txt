grep
====

**Example file** 

`wget https://www.apache.org/licenses/LICENSE-2.0.txt`

-------------------  

**note** multiple flags can be added together here they are seperate to make it easier to understand.

e.g. grep -oic is the same as grep -o -i -c

--------------------------------  
>grep options *pattern* input\_file\_names


find the word **law** in LICENSE-2.0.txt

`grep law LICENSE-2.0.txt`  

> cross-claim or counterclaim in a **law**suit) alleging that the Work  
Disclaimer of Warranty. Unless required by applicable **law** or  
unless required by applicable **law** (such as deliberate and grossly  
Unless required by applicable **law** or agreed to in writing, software  


find whole words

`grep -w`  

--word-regexp
    Select only those lines containing matches that form whole words.

`grep -w "law" LICENSE-2.0.txt`  
>    Disclaimer of Warranty. Unless required by applicable **law** or  
      unless required by applicable **law** (such as deliberate and grossly  
       nless required by applicable **law** or agreed to in writing, software  


case insensitive search

`grep -i`

 --ignore-case
    Ignore case distinctions in both the PATTERN and the input files

count occurrences of match 

`grep -c`  

--count
    Suppress normal output; instead print a count of matching lines for each input file

`grep -c "you" LICENSE-2.0.txt `    

>6

`grep -i -c "you" LICENSE-2.0.txt `  

>34

use case insensitive and whole words

`grep -i -w -c "you" LICENSE-2.0.txt `  
>26


show lines after match 

`grep -A {n}` where n is the number of lines to print after

--after-context=NUM
    Print NUM lines of trailing context after matching lines

`grep -A 1 -i "medium" LICENSE-2.0.txt `  

>Work or Derivative Works thereof in any **medium**, with or without  
modifications, and in Source or Object form, provided that You

show lines before match

`grep -B {1}` where n is the number of lines to print before  

show both before and after lines

`grep -C {n}` where n is the number of lines to print before and after. 


find parts of sentance 

`grep "Work shall" LICENSE-2.0.txt`

>granted to You under this License for that **Work shall** terminate

looking for a match over multiple documents? 

`grep -r` 

-r, --recursive
    Read all files under each directory, recursively

just show the file names that contain the match

--files-with-matches
    Suppress normal output; instead print the name of each input file  

`grep -l -i "medium" LICENSE-2.0.txt`  

>LICENSE-2.0.txt

by default grep will show the entire line. To see only the match

--only-matching
    Print only the matched (non-empty) parts of a matching line

`grep -o -i "medium" LICENSE-2.0.txt `  
>medium

display line numbers of match 

--line-number
    Prefix each line of output with the 1-based line number within its input file

`grep  -n -i "medium" LICENSE-2.0.txt `  
>**91:**      Work or Derivative Works thereof in any medium, with or without



## regular expressions

to make this easier remove all the whitespace from beginning of the file

`sed -i 's/^ *//' LICENSE-2.0.txt` 

special characters 

^ beginning of line  
$ end of line

`grep  "^on" LICENSE-2.0.txt `  
>**on** behalf of whom a Contribution has been received by Licensor and  
**on** Your own behalf and on Your sole responsibility, not on behalf  

`grep  -w "or$" LICENSE-2.0.txt `  
>direction or management of such entity, whether by contract **or**  
"Work" shall mean the work of authorship, whether in Source **or**  
(a) You must give any other recipients of the Work **or**  
as part of the Derivative Works; within the Source form **or**  
for use, reproduction, or distribution of Your modifications, **or**  
7. Disclaimer of Warranty. Unless required by applicable law **or**  

inverse (make more sense with regular expressions)

`grep -v`

--invert-match
    Invert the sense of matching, to select non-matching lines

find all lines that are not empty 

`grep -v -c ^$ LICENSE-2.0.txt `
>169


find all lines that are empty 

`grep -c ^$ LICENSE-2.0.txt `  
>33

using "." to match any character 

`grep -o mission LICENSE-2.0.txt `    
>mission  
mission  
mission  
mission  
mission  
`grep -o ..mission LICENSE-2.0.txt `  
>ermission  
ubmission  
ermission  
ermission  
ermission  
`grep -o ...mission LICENSE-2.0.txt `  
>permission  
Submission  
permission  
permission  
permission  


what if you actually want to search for the charater . 

`grep -F`  

--fixed-strings
    Interpret PATTERN as a list of fixed strings

`grep 2.0 LICENSE-2.0.txt `  
Version **2.0**, January **200**4  
Licensed under the Apache License, Version **2.0** (the "License");  
http://www.apache.org/licenses/LICENSE-**2.0**  

`grep -F 2.0 LICENSE-2.0.txt `  
Version **2.0**, January 2004  
Licensed under the Apache License, Version **2.0** (the "License");  
http://www.apache.org/licenses/LICENSE-**2.0**


grep OR 

find lines with one of two more or matches

`grep -i -E "medium|bracket" LICENSE-2.0.txt `  
>Work or Derivative Works thereof in any **medium**, with or without  
boilerplate notice, with the fields enclosed by **bracket**s "[]"  
the **bracket**s!)  The text should be enclosed in the appropriate  

or use egrep 

`egrep -i  "medium|bracket" LICENSE-2.0.txt `  


grep AND  

find all lines containing "to" and "the" in that order  
`grep -E -w 'to.*the' LICENSE-2.0.txt`

find all lines containing "to" and "the" in any order  
`grep -E -i 'to.*the|the.*to' LICENSE-2.0.txt`  